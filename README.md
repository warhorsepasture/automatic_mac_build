# Automatic Mac Build

Ansible playbooks for configuring my mac. Draws heavily from [Jeff Geerling's work](https://github.com/geerlingguy/mac-dev-playbook)

## Dev/Test

Some help when you want to develop and test these playbooks.

### Using a VM for dev

To build a VM from scratch:
0. Install Parallels.
1. Create a MacOS VM in Parallels.
    1. Click plus sign in top right of Parallels.
    2. Download MacOS.
    3. Setup user and password.
2. Install Parallels toolbox.
    1. Once the guest is running, log in.
    2. On the host, in Parallels menubar -> Parallels Desktop —> Install Parallel Toolbox for Mac...
3. Increase the VMs specs. (otherwise xcode download is impossibly slow)
    1. Shutdown the guest.
    2. `prlctl  set GeerlingX --memsize 8192 --cpus 4`
4. Clone the machine - so you can go back to the base MacOS machine later.
    2. `prlctl list -a`
    3. Choose the one Parallels just created. Probably ‘MacOS 12’
    4. `prlctl “MacOS 12” --name “devtestNNN”

Notes:
It is helpful to have a macOS VM for development and testing. That way:
1. I don't have to check the code in on one machine just to deploy it on another.
2. I can fully test the results.

Jeff Geerling tests with [Github Actions](https://github.com/geerlingguy/mac-dev-playbook#testing-the-playbook). I would love
to do that myself, but I don't know how. In the meantime, I just use a VM.

I use Parallels to host a macOS VM on my macOS machine. I've had some trouble with it, including VMs that suddenly won't boot
and my host mac crashing after waking up. Also, Apple doesn't let you install your Apple ID on VMs, so I can't do and build 
all parts of my setup. But it is the best solution I found.

## Pre-requisites

Before running the plays, you must manually install ansible and this repository.

### Install Ansible
```
xcode-select --install` to launch the installer;

# we use python3.9 because that's what xcode installs
export PATH="$HOME/Library/Python/3.9/bin:/opt/homebrew/bin:$PATH";

sudo pip3 install --upgrade pip;
pip3 install ansible;
```

### Install this repository
```
mkdir ~/.config;
cd ~/.config;
git clone https://gitlab.com/warhorsepasture/automatic_mac_build.git;
cd automatic_mac_build;
ansible-galaxy install -r requirements.yml;
```

## Configure the Mac 

### Run initial configuration

```
cd ~/.config/automatic_mac_build;
export PATH="$HOME/Library/Python/3.9/bin:/opt/homebrew/bin:$PATH";
ansible-playbook main.yml --ask-become-pass --tags "[]"
```


### Manual steps

After running the playbook, you have to complete the following manual steps

Dropbox
- sign in
- ensure that at least the XXX directory is syncing
- 

Firefox
- sign in

Chrome
- enable installed extensions
  - click on puzzle piece
  - enable

Settings
- Privacy → Apple Advertising → Personalized Ads → Uncheck


Allow Full Disk Access for
- Knock Knock
- Block Block




## Other notes

I use homebrew to install many of applicaitons I use. See the files in the ansible role for the latest lists. Note that this page has a list of GUI applications available for install via cask: https://formulae.brew.sh/cask/

## Firefox
I monkeyed with firefox preferences, so I want to document it here. Firefox sync will sync preferences set in about:config, but only if that preference is whitelisted by having a corresponding entry under services.sync.prefs.sync. Additionally, the preferences are not synced automatically unless the preference is already set on the device. That makes it hard to set up new machines. The work around is also setting services.sync.prefs.dangerously_allow_arbitrary to true.

For example, when I open a bookmark, I want it to open in a new tab. That is controlled by a preference called browser.tabs.loadBookmarksInTabs. That preference is not automatically synced. So I,
1. set browser.tabs.loadBookmarksInTabs=true
2. set services.sync.prefs.sync.browser.tabs.loadBookmarksInTabs=true
3. set services.sync.prefs.dangerously_allow_arbitrary=true

Read more
https://support.mozilla.org/en-US/kb/sync-custom-preferences

The alternatie to all this is to set prefs in a user.js file and sync that file myself.  fuck that noise.


Plex
Networking
sudo networksetup -setadditionalroutes Ethernet 192.168.0.0 255.255.255.0 192.168.0.1
https://apple.stackexchange.com/questions/307221/add-a-permanent-static-route-in-high-sierra
#### Steps on a physical machine that supports 1Password
1. Download and install 1PW.
2. Configure my user.
3. Preferences.
    1. Appearance -> Always show in sidebar -> Categories
    2. Security -> Always show passwords and full credit card numbers
    3. Developer -> Use SSH agent
    4. Copy snippet to .ssh/config
